# FoundryVTT dnd5e lang fr-FR

## Ce module traduit les clés (termes) exposés par le créateur pour ce system de jeu DND5E. 
		
## Installation
	Dans le menu des modules, cliquer sur install module" et dans le champs Manifest, copier le lien ci-dessous
	 https://gitlab.com/baktov.sugar/foundryvtt-dnd5e-lang-fr-fr/raw/master/dnd5e_fr-FR/module.json  
	Une fois votre Monde lancé, n'oubliez pas d'activer le module et de définir la langue Française pour ce monde.	

## Nous rejoindre
1. Vous pouvez retrouver toutes les mises à jour en français pour FoundryVTT sur le discord officiel francophone
2. Lien vers [Discord Francophone](https://discord.gg/pPSDNJk)
			